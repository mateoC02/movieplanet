<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRowSeatingsCinema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('row_seatings_cinema', function (Blueprint $table) {
            $table->id();
            $table->string("row",10);
            $table->unsignedBigInteger("id_cinema");

            $table->foreign("id_cinema")
                  ->references("id")
                  ->on("cinemas")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('row_seatings_cinema');
    }
}
