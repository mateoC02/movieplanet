<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatingsReservedMoviePremierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seatings_reserved_movie_premier', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_seating_for_row_cinema");
            $table->unsignedBigInteger("id_movie_premier");
            $table->unsignedBigInteger("id_user");
            $table->unsignedBigInteger("id_payment_detail_card");

            $table->foreign("id_seating_for_row_cinema","seatings_reserved_for_row_cinema")
                  ->references("id")
                  ->on("seatings_for_row_cinema")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_movie_premier")
                  ->references("id")
                  ->on("movie_premier")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_user")
                  ->references("id")
                  ->on("user")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_payment_detail_card")
                  ->references("id")
                  ->on("detail_card")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seatings_reserved_movie_premier');
    }
}
