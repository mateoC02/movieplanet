<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_card', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_type_card");
            $table->integer("card");
            $table->string("expiration-date",10);
            $table->integer("ccv");

            $table->foreign("id_type_card")
                  ->references("id")
                  ->on("type_card")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_card');
    }
}
