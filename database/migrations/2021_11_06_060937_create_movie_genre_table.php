<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_genre', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_movie");
            $table->unsignedBigInteger("id_genre");

            $table->foreign("id_movie")
                  ->references("id")
                  ->on("movies")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_genre")
                  ->references("id")
                  ->on("genre")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_genre');
    }
}
