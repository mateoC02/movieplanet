<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviePremierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_premier', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_movie");
            $table->unsignedBigInteger("id_cinema");
            $table->string("price",10);
            $table->date("release-date");

            $table->foreign("id_movie")
                  ->references("id")
                  ->on("movies")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_cinema")
                  ->references("id")
                  ->on("cinemas")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_premier');
    }
}
