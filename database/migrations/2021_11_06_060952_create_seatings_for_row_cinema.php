<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatingsForRowCinema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seatings_for_row_cinema', function (Blueprint $table) {
            $table->id();
            $table->string("row",10);
            $table->unsignedBigInteger("id_row_cinema");

            $table->foreign("id_row_cinema")
                  ->references("id")
                  ->on("row_seatings_cinema")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seatings_for_row_cinema');
    }
}
