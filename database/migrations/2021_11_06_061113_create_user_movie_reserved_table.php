<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMovieReservedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_movie_reserved', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_user");
            $table->unsignedBigInteger("id_seating_reserved_movie_premier");
            
            $table->foreign("id_user")
                  ->references("id")
                  ->on("user")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

            $table->foreign("id_seating_reserved_movie_premier")
                  ->references("id")
                  ->on("seatings_reserved_movie_premier")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_movie_reserved');
    }
}
