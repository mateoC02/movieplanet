<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActorMovieRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actor_movie_role', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_movie_premier");
            $table->unsignedBigInteger("id_actor");
            $table->string("role");

            $table->foreign("id_movie_premier")
                  ->references("id")
                  ->on("movie_premier")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");
            
            $table->foreign("id_actor")
                  ->references("id")
                  ->on("actors")
                  ->onDelete("cascade")
                  ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actor_movie_role');
    }
}
