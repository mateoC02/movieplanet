<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <title>NAME PELICULA</title>

</head>
<body class="h-screen " >

  <header class="absolute w-full z-10" >
    <div class="container mx-auto p-4">
        
        <section class="grid grid-cols-2" >
            <div>
                  <h2 class="text-white text-2xl" > <a href="/"> MOVIES <span class="font-bold" >PLANET</span> </a></h2>
            </div>

            <div class="flex justify-end gap-4" >
                
                 <h2 class="text-white text-2xl" >LOGAN </h2>
                
            </div>
        </section>
    </div>
</header>

    <section class="w-full shadow-inner relative">

        <main class="w-screen h-screen bg-gray-100 flex justify-center items-center">
            
            <img class="w-full absolute object-cover object-bottom h-screen" src="https://images4.alphacoders.com/844/844633.jpg" alt="">
            <div class="w-screen h-screen absolute top-0 opacity-30 bg-gray-800"></div>

            <div class="steps relative z-20 shadow-lg rounded-xl bg-gray-100" style="width: 850px; height: 850px;" >
                
                <div class="step active p-5" id="step1">
                    
                    <h2 class="text-center font-bold text-3xl my-4">Escoga su asiento</h2>

                    <div class="w-full flex" >
                        <ul class="w-full">
                            <li class="flex items-center text-gray-600" > <div class="bg-red-500 rounded mr-3" style="width: 10px; height: 10px;" ></div> Asientos no disponibles </li>
                            <li class="flex items-center text-gray-600" > <div class="bg-yellow-500 rounded mr-3" style="width: 10px; height: 10px;" ></div> Asientos Reservados </li>
                            <li class="flex items-center text-gray-600" > <div class="bg-gray-100 shadow border-2 rounded mr-3" style="width: 10px; height: 10px;" ></div> Asientos disponibles </li>
                        </ul>
                        <p class="font-bold">Precio: S/.20</p>
                    </div>

                    <div class="w-full overflow-y-scroll" >
                       
                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">G</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 bg-red-500 text-white shadow-lg rounded-lg flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>

                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">F</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-green-300 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>

                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">E</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-yellow-400 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>
                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">D</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>

                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">C</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>

                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">B</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>

                        <div class="grid grid-cols-12 gap-3 rounded-lg shadow bg-blue-100 my-4">
                            
                            <div class="col-span-1 flex items-center rounded shadow-lg justify-center bg-gray-800 text-white font-bold h-full">A</div>

                            <div class="grid grid-cols-12 col-span-11">
                                
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">11</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">10</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">9</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">8</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">7</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">6</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">5</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">4</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">3</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">2</div>
                                <div class="m-3 px-6 py-4 shadow-lg rounded-lg bg-gray-100 flex justify-center">1</div>
                               
                            </div>
                        
                        </div>
                     
                    </div>

                    
                    <div class="button-zone w-full flex justify-end">
                        <button class="button-step primary border-2 border-gray-800 px-6 py-3 rounded font-bold text-white" data-step="#step1" data-next="#step2">Siguiente</button>
                    </div>
                </div>
    
                <div class="step p-5" id="step2">
                     
                    <h2 class="text-center font-bold text-6xl my-4">Escoga una Forma de Pago</h2>
                    
                    <div class="flex justify-around my-6" >
                        <div class="rounded shadow py-5 px-16 bg-white flex gap-4">
                            <img src="https://logos-marcas.com/wp-content/uploads/2020/04/Visa-Emblema.png" width="50px" alt="">
                        </div>
                        <div class="rounded shadow py-5 px-16 bg-white flex gap-4">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/MasterCard_Logo.svg/2560px-MasterCard_Logo.svg.png" width="50px" alt="">
                        </div>
                    </div>

                    <div class="w-full" >
                        <form class="flex flex-wrap gap-3 w-full p-5">
                            <label class="relative w-full flex flex-col">
                              <span class="font-bold mb-3">Numero De Tarjeta</span>
                              <input class="rounded-md peer pl-12 pr-2 py-2 border-2 border-gray-200 placeholder-gray-300" type="text" name="card_number" placeholder="0000 0000 0000" />
                              <svg xmlns="http://www.w3.org/2000/svg" class="absolute bottom-0 left-0 -mb-0.5 transform translate-x-1/2 -translate-y-1/2 text-black peer-placeholder-shown:text-gray-300 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                              </svg>
                            </label>
                          
                            <label class="relative flex-1 flex flex-col">
                              <span class="font-bold mb-3">Fecha de Expiracion</span>
                              <input class="rounded-md peer pl-12 pr-2 py-2 border-2 border-gray-200 placeholder-gray-300" type="text" name="expire_date" placeholder="MM/YY" />
                              <svg xmlns="http://www.w3.org/2000/svg" class="absolute bottom-0 left-0 -mb-0.5 transform translate-x-1/2 -translate-y-1/2 text-black peer-placeholder-shown:text-gray-300 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                              </svg>
                            </label>
                          
                            <label class="relative flex-1 flex flex-col">
                              <span class="font-bold flex items-center gap-3 mb-3">
                                CVC/CVV
                                <span class="relative group">
                                  <span class="hidden group-hover:flex justify-center items-center px-2 py-1 text-xs absolute -right-2 transform translate-x-full -translate-y-1/2 w-max top-1/2 bg-black text-white"> Hey ceci est une infobulle !</span>
                                  <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                  </svg>
                                </span>
                              </span>
                              <input class="rounded-md peer pl-12 pr-2 py-2 border-2 border-gray-200 placeholder-gray-300" type="text" name="card_cvc" placeholder="&bull;&bull;&bull;" />
                              <svg xmlns="http://www.w3.org/2000/svg" class="absolute bottom-0 left-0 -mb-0.5 transform translate-x-1/2 -translate-y-1/2 text-black peer-placeholder-shown:text-gray-300 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                              </svg>
                            </label>
                          </form>
                    </div>
                    <div class="button-zone w-full flex justify-end">
                        <button class="button-step border-1 bg-primary px-6 py-3 rounded font-bold text-white" data-step="#step2" data-next="#step3">Siguiente</button>
                    </div>
                </div>
    
                <div class="step" id="step3">
                    
                    <div class="w-full">
                        <img class="w-full h-full" src="https://marketingweek.imgix.net/content/uploads/2020/05/20125057/shutterstock_586719869.jpg" alt="">
                    </div>

                    <div class="m-4 relative">
                        <h2 class="font-bold text-center text-xl my-3" >Gracias por su Compra!</h2>
                        <ul>
                            
                            <li class="flex" >
                                <p class="font-bold" >Pelicula:</p> <span class="text-semibold mx-2 text-gray-700"> Logan </span> 
                            </li>


                            <li class="flex" >
                                <p class="font-bold" >Asiento:</p> <span class="text-semibold mx-2 text-gray-700"> 10 </span> 
                            </li>


                            <li class="flex" >
                                <p class="font-bold" >Fila:</p> <span class="text-semibold mx-2 text-gray-700"> A </span> 
                            </li>


                            <li class="flex" >
                                <p class="font-bold" >Tipo de Pago:</p> <span class="text-semibold mx-2 text-gray-700"> Visa </span> 
                            </li>

                        </ul>

                        <div>
                            <img src="" alt="">
                        </div>

                    </div>

                    <div class="button-zone">
                        <button class="button-step"  data-step="#step3" data-next="finish">Finalizar Compra</button>
                    </div>
    
                </div>
    
            </div>
        </main>
    </section>

    <script>
        const getElement  = (el) => document.querySelector(el);
        const getElements = (el) => document.querySelectorAll(el);
        const execTime = 200;

        function initStep(){
            [...getElements(".button-step")].map( 
                (button) => button.addEventListener("click",() => nextStep({ current: button.dataset.step, next: button.dataset.next }),false)
            );
        }

        function nextStep(step) {
            
            if (step.next == 'finish' ) {
                finishUp();
                return;
            }
            
            getElement(step.current).classList.add("comeOut");
            setTimeout(() => {
                
                getElement(step.current).classList.remove("comeOut");
                getElement(step.current).classList.remove("active");
                getElement(step.next).classList.add("comeOn");
                
                setTimeout(() => {
                    getElement(step.next).classList.add("active");
                    setTimeout(() => getElement(step.next).classList.remove("comeOn"), execTime);
                }, execTime / 2);

            }, execTime);
            
        }

        function finishUp(){
            alert('finish')
        }

        initStep()
      
    </script>
</body>
</html>