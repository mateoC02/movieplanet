<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="MoviePlanet es un cine con las mejores peliculas en estreno para toda la familia">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>MOVIEPLANET - las mejores peliculas en estreno</title>
</head>
<body>

    @php
        
        $routesNotHeader = ["user","signup","login"];
        $isRoute = false;
        foreach($routesNotHeader as $route){
            if(Request::is($route)){
                $isRoute = true;
                break;
            }
        }

    @endphp

    @if(!$isRoute)
        @include('templates.header')
    @endif
    
    @section('main-content')
    @show

</body>
</html>