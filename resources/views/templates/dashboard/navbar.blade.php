<nav class="side-navbar">
    
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar"><img src="/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h4">Mark Stephen</h1>
            <p>Administrador</p>
        </div>
    </div>
    
    <span class="heading">Menu</span>
    <ul class="list-unstyled">
        <li class="{{ Request::is('admin') ? 'active' : '' }}"        ><a href="{{ route('admin') }}"> <i class="fa fa-home"></i>Home </a></li>
        <li class="{{ Request::is('admin/movies') ? 'active' : '' }}" ><a href="{{ route('movies') }}"> <i class="fa fa-film"></i>Peliculas </a></li>
        <li class="{{ Request::is('admin/genres') ? 'active' : '' }}" ><a href="{{ route('genres') }}"> <i class="fa fa-tag"></i>generos </a></li>
        <li class="{{ Request::is('admin/premier') ? 'active' : ''}}" ><a href="{{ route('premier')}}"> <i class="fa fa-film"></i>Estreno </a></li>
        <li class="{{ Request::is('admin/cinema') ? 'active' : '' }}" ><a href="{{ route('cinema') }}"> <i class="fa fa-television"></i>Salas </a></li>
        <li class="{{ Request::is('admin/actors') ? 'active' : '' }}" ><a href="{{ route('actors') }}"> <i class="fa fa-star"></i>Actores </a></li>
        <li class="{{ Request::is('admin/users') ? 'active' : ''  }}" ><a href="{{ route('users')  }}"> <i class="fa fa-users"></i>Usuarios </a></li>
    </ul>
   
</nav>