<header class="absolute w-full z-10" >
    <div class="container mx-auto p-4">
        
        <section class="grid grid-cols-2" >
            <div>
                <h2 class="text-white text-2xl" > <a href="/"> MOVIES <span class="font-bold" >PLANET</span> </a></h2>
            </div>

            <div class="flex justify-end gap-4" >
                
                <div class="flex gap-4" >
                    <button class="p-2 text-white" >Iniciar Sesion</button>
                    <button class="rounded bg-primary font-semibold text-white p-2" >Registrarse</button>
                </div>
                
            </div>
        </section>
    </div>
</header>