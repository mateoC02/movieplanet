<div id="modal" class="no-events fixed z-10 inset-0 overflow-y-auto ease-out duration-300 opacity-0" aria-labelledby="modal-title" role="dialog" aria-modal="true">
    <div class="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
    
        <div id="close" class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div id="content" class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle  ease-out duration-300 opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
            <div class="bg-white">
                
                <div class="grid grid-cols-2 justify-between" style="height: 550px; width: 750px;" >
                    
                    <div class="flex justify-between w-full" >
                        <form class="mt-6 w-full p-5">
                            <h3 class="my-5 font-bold text-3xl" >Iniciar Sesion</h3>
                            <div class="mb-8" >
                                <label for="email" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Usuario</label>
                                <input id="email" type="email" name="email" placeholder="myuser.05" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
                            </div>
                            
                            <div class="mb-3" >
                                <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Contraseña</label>
                                <input id="password" type="password" name="password" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
                            </div>
                            
                            <button type="submit" class="rounded w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-primary shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
                            Iniciar Sesion
                            </button>
                            <p class="flex justify-between mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Ya tienes una cuenta?</p>
                        </form>
                    </div>
                    

                    <div class="h-full bg-black md:flex gap-2 p-4 flex-col justify-center items-center " >
                        <h2 class="text-white text-5xl text-center" >MOVIE <span class="font-bold" >PLANET</span> </h2>
                        <p class="text-white font-light text-sm" >las mejores peliculas en estreno</p>
                    </div>

                </div>
            </div>                
        </div>
    </div>
</div>


<script>

    let modal   = document.getElementById('modal');
    let content = document.getElementById('content');
    let open    = document.querySelectorAll('.open');
    let close   = document.getElementById('close');

    [...open].map( button => {
        button.addEventListener('click',(e)=>{
            modal.classList.add("opacity-100","events","ease-out","duration-300")
            content.classList.add("opacity-100", "translate-y-0", "sm:scale-100");
        });
    })

    close.addEventListener('click',()=>{
        content.classList.remove("opacity-100", "translate-y-0", "sm:scale-100");
        modal.classList.remove("opacity-100","events","ease-out","duration-300")
        modal.classList.add("ease-in","duration-200")
    })

</script>