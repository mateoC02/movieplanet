

@extends('templates.skeleton')

@section('main-content')
 <section class="w-full shadow-inner relative">

        <img class="w-full object-cover object-bottom h-screen" src="https://images4.alphacoders.com/844/844633.jpg" alt="">
        <div class="w-screen h-screen absolute top-0 opacity-30 bg-gray-800"></div>
        
        <div class="absolute top-0 h-full w-full p-8 text-white container mx-auto">
            
            <div class="h-full flex flex-col justify-center" >
                <h2 class="text-white font-bold text-6xl mb-10">LOGAN</h2>
                
                <div class="flex gap-6 mb-10" >
                    <p class="text-gray-200" >Fantasia</p>
                    <p class="text-gray-200" >Animacion</p>
                    <p class="text-gray-200" >Terror</p>
                    <p> | </p>
                    <p class="text-gray-200" > Duration: 1h 52min </p>
                </div>
    
                <p style="max-width: 750px;" class="my-6" > Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatem rerum sint et modi maiores autem, architecto neque veniam qui alias atque voluptates doloribus, ratione fugiat esse perferendis explicabo provident itaque. Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus repellendus sapiente, nesciunt consequuntur obcaecati, iure quo atque ex at perspiciatis ratione autem quaerat alias, necessitatibus eveniet explicabo ullam? Laudantium, ullam!</p>
                
                <div class="flex gap-6 items-center" >
                    <button class="open bg-primary font-semibold text-white px-4 py-2" >Reservar Asiento</button>
                    <button class="border-2 border-white font-semibold text-white px-4 py-2" >Ver Trailer</button>                
                    <p>Se estrena en 4 dias</p>
                </div>


            <div class="flex mt-10" style="height: 190px; " >
                <div class="h-full" >
                    <h4 class="text-white font-bold text-2xl mb-3">REPARTO</h4>
                    <div class="flex justify-left items-center h-full mt-10" >
                            
                        <div style="width: 160px; height: 250px;" class="flex justify-center flex-col m-5 h-full" >
                            <img class=" shadow bg-white object-cover rounded-lg h-full"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLJ9DkN4vV1S7gmSY_3pvtNdNicbcRV2Q8EXnvkrNw6nD4scJMpUKWus6T00fuSRn36TM&usqp=CAU" alt="">
                            <p class="text-center font-bold" >Nombre del Actor</p>
                            <p class="text-center font-light" >Papel</p>
                        </div>
                       
                        <div style="width: 160px; height: 250px;" class="flex justify-center flex-col m-5 h-full" >
                            <img class=" shadow bg-white object-cover rounded-lg h-full"  src="https://es.web.img3.acsta.net/pictures/19/11/12/17/00/5910399.jpg" alt="">
                            <p class="text-center font-bold" >Nombre del Actor</p>
                            <p class="text-center font-light" >Papel</p>
                        </div>
                       
                        <div style="width: 160px; height: 250px;" class="flex justify-center flex-col m-5 h-full" >
                            <img class=" shadow bg-white object-cover rounded-lg h-full"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs12nyxewClcHy1eGkI1AGvkShRa0No9-E--38zSOXg6mfrzMVjstmXMRHiS0GB5SSQUE&usqp=CAU" alt="">
                            <p class="text-center font-bold" >Nombre del Actor</p>
                            <p class="text-center font-light" >Papel</p>
                        </div>
                       
                        <div style="width: 160px; height: 250px;" class="flex justify-center flex-col m-5 h-full" >
                            <img class=" shadow bg-white object-cover rounded-lg h-full"  src="https://es.web.img3.acsta.net/pictures/19/09/19/02/15/1471670.jpg" alt="">
                            <p class="text-center font-bold" >Nombre del Actor</p>
                            <p class="text-center font-light" >Papel</p>
                        </div>
                       
                        
                    </div>
                </div>
             </div>

            

            </div>
           

        </div>

        <!-- <div class="absolute bottom-0 right-16 mb-16" >
            <iframe  class="rounded-xl" width="750" height="450" src="https://www.youtube.com/embed/Div0iP65aZo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
         -->
    </section>
@endsection
