
@extends('templates.skeleton')

@section('main-content')
<section class="w-full shadow-inner relative">

        <img class="w-full object-cover object-bottom" style="height: 750px;" src="https://wallpaperaccess.com/full/3428791.jpg" alt="">
        <div class="absolute bottom-0 w-full p-8 text-white container mx-auto">
            
            <h2 class="text-white font-bold text-6xl mb-10">WRATH OF THE TITANS</h2>
            <div class="flex gap-6 mb-10" >
                <p class="text-gray-200" >Fantasia</p>
                <p class="text-gray-200" >Animacion</p>
                <p class="text-gray-200" >Terror</p>
                <p> | </p>
                <p class="text-gray-200" > Duration: 1h 52min </p>
            </div>

            <div class="flex gap-6 items-center" >
                <button class="open bg-primary font-semibold text-white px-4 py-2" >Reservar Asiento</button>
                <button class="border-2 border-white font-semibold text-white px-4 py-2" >Mas Informacion</button>
                <p>Se estrena en 4 dias</p>
            </div>

        </div>

    </section>

    <main class="w-full" >
        <div class="container p-5 mx-auto">
            
            <div class="flex justify-between items-center" >
                <section class="flex gap-6 my-6 px-8" >
               
                    <div>
                        <p class="primary" >En estreno</p>
                        <p class="w-full bg-pink-600 rounded" style="height: 4px;" ></p>
                    </div>
                    
                    <p class="" >Proximamente</p>
                </section>

                <section class="relative" >
                    <div class="flex w-full" >
                        <input type="text" placeholder="buscar..." style="width: 450px;" class=" p-2 rounded-lg border-2 border-gray-300">
                        <svg class="w-6 h-6 text-gray-400 absolute top-3 right-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                    </div>
                
                </section>
            </div>
            

            <section class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 justify-center items-center " >
               
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="https://sm.ign.com/ign_es/movie/l/logan/logan_jrbx.jpg" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >Logan</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="flex mt-4" >
                                <button class="open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
        
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="https://images.jumpseller.com/store/la-fortaleza-punta-arenas1/9942725/vengadores-preludio-avengers-preludio.jpg?1621881877" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >The Avengers</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="flex mt-4" >
                                <button class="open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
        
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="https://es.web.img2.acsta.net/pictures/17/06/19/14/01/130456.jpg" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >Spiderman Homecoming</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="w-full mt-4" >
                                <button class="w-full open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="w-full border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="https://pbs.twimg.com/media/EJmltx0XkAE2uvC.jpg" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >El Hoyo</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="w-full mt-4" >
                                <button class="w-full open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="w-full border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="http://2.bp.blogspot.com/-2oQbeVm_enw/VMqr_v1bccI/AAAAAAAAAu4/W8idjQLpOOk/s1600/11172322_800.jpg" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >Evil Dead 2013</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="w-full mt-4" >
                                <button class="w-full open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="w-full border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="contain-card w-full flex justify-center">
                    <div class="card rounded-lg shadow mb-6" style="max-width: 300px;" >
                        <div>
                            <img class="rounded" src="https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2017/09/saw-viii_4.jpg?itok=9UJpQe7-" alt="">
                        </div>
                        <div class="p-4" >
                            <div class="flex items-center justify-between mb-4" >
                                <h3 class="font-bold text-xl" >Jiwsag</h3>
                                <span class="text-gray-700" > estreno en 3 dias </span>
                            </div>
                            <p class="text-gray-600" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quaerat nisi sed ...</p>
                            <div class="flex mt-4" >
                                <button class="open bg-primary font-semibold text-white rounded shadow text-sm px-2  m-1 py-2" >Reservar Asiento</button>
                                <button class="border-2 border-black font-semibold  rounded shadow text-sm px-2 m-1  py-2" >Mas Informacion</button>
        
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>

        </div>
    </main>
@endsection