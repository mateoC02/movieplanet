@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner">
	<div class="content-inner p-5">

  	<div class="col-lg-12">
      <div class="card">
        <div class="card-close">
          
        </div>
        <div class="card-header d-flex align-items-center">
          <h3 class="h4">NUEVO ESTRENO</h3>
        </div>
        <div class="card-body">
          <form class="form-horizontal">
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">pelicula</label>
              <div class="col-sm-9">
                <select class="form-control">
                	<option value="">pelicula 1</option>
                	<option value="">pelicula 1</option>
                	<option value="">pelicula 1</option>
                </select>
              </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Fecha de estreno</label>
              <div class="col-sm-9">
                <input class="form-control" type="date">
              </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">sala</label>
              <div class="col-sm-9">
                <select class="form-control">
                	<option value="">SALA 1</option>
                	<option value="">SALA 1</option>
                	<option value="">SALA 1</option>
                </select>
              </div>
            </div>

            
            <div class="d-flex justify-content-end w-100">
               <button type="submit" class="btn btn-success" > Guardar </button>
            </div>

          </form>
        </div>
      </div>
    </div>
</div>
</div>
@endsection