@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner p-5">

    <div class="col-lg-12">
      <div class="card">
        <div class="card-close">
          
        </div>
        <div class="card-header d-flex align-items-center">
          <h3 class="h4">NUEVO GENERO</h3>
        </div>
        <div class="card-body">
          <form class="form-horizontal">
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">genero</label>
              <div class="col-sm-9">
                <input type="text" class="form-control">
              </div>
            </div>            

            <div class="d-flex justify-content-end w-100">
               <button type="submit" class="btn btn-success" > Guardar </button>
            </div>

          </form>
        </div>
      </div>
    </div>
</div>
@endsection