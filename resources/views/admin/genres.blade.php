@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner">
	  <div class="row p-5">
    <div class="card w-100">
        
        <div class="card-header d-flex align-items-center justify-content-between">
          <h3 class="h4">GENEROS</h3>
          
          <button class="btn btn-success flex">
            <a href="{{ route('newGenres') }}" class="text-white" >
              <i class="fa fa-plus"></i> nuevo  
            </a>
          </button>
        
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table overflow-scroll" style="max-height: 700px;" >
              <thead>
                <tr>
                  <th>nombre</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>

                  <td>Accion</td>
					
					<td>
	                    <button class="btn btn-primary" >
	                      <i class="fa fa-edit"></i>  
	                    </button>

	                    <button class="btn btn-danger" >
	                      <i class="fa fa-trash-o"></i>  
	                    </button>
	                    
                  	</td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>              
  </div>
</div>
@endsection