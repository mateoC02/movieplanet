@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner p-5">

  	<div class="col-lg-12">
      <div class="card">
        <div class="card-close">
          
        </div>
        <div class="card-header d-flex align-items-center">
          <h3 class="h4">NUEVA SALA DE CINE</h3>
        </div>
        <div class="card-body">
          <form class="form-horizontal">
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Nombre</label>
              <div class="col-sm-9">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Asientos</label>
              <div class="d-flex">
                <input type="text" class="mx-5 form-control" placeholder="fila">
                <input type="number" max="11" min="11" class="mx-5 form-control" placeholder="asientos">
                <button type="submit" class="mx-5 btn btn-success w-100" > Agregar Fila </button>
              </div>
            </div>
            

            <div class="d-flex justify-content-end w-100">
               <button type="submit" class="btn btn-success" > Guardar </button>
            </div>

          </form>
        </div>
      </div>
    </div>
</div>
@endsection