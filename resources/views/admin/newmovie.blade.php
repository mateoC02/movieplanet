@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner p-5">

  	<div class="col-lg-12">
      <div class="card">
        <div class="card-close">
          
        </div>
        <div class="card-header d-flex align-items-center">
          <h3 class="h4">NUEVA PELICULA</h3>
        </div>
        <div class="card-body">
          <form class="form-horizontal">
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Nombre</label>
              <div class="col-sm-9">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Descripcion</label>
              <div class="col-sm-9">
                <textarea class="form-control" style="width: 100%;" > </textarea>
              </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">duracion</label>
              <div class="col-sm-9">
                <input type="time" class="form-control">
              </div>
            </div>

            <div class="line"></div>
            
            <div class="form-group row">
              <label for="fileInput" class="col-sm-3 form-control-label">portada</label>
              <div class="col-sm-9">
                <input id="fileInput" type="file" class="form-control-file">
              </div>
            </div>

            <div class="line"></div>
            
            <div class="form-group row">
              <label for="fileInput" class="col-sm-3 form-control-label">cover</label>
              <div class="col-sm-9">
                <input id="fileInput" type="file" class="form-control-file">
              </div>
            </div>

          
            <div class="line"></div>
            
            <div class="form-group row">
              <label for="fileInput" class="col-sm-3 form-control-label">Actores</label>
              <div class="col-sm-9">
                <button type="submit" class="btn btn-success" > Agregar Actor </button>
                <div class="my-4" >
                  <ul>
                    <li>Jose gutierrez - Logan</li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="d-flex justify-content-end w-100">
               <button type="submit" class="btn btn-success" > Guardar </button>
            </div>

          </form>
        </div>
      </div>
    </div>
</div>
@endsection