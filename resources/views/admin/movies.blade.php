@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner">
  
  <div class="row p-5">
    <div class="card w-100">
        
        <div class="card-header d-flex align-items-center justify-content-between">
          <h3 class="h4">PELICULAS</h3>
          
          <button class="btn btn-success flex">
            <a href="{{ route('newMovie') }}" class="text-white" >
              <i class="fa fa-plus"></i> nuevo  
            </a>
          </button>
        
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table overflow-scroll" style="max-height: 700px;" >
              <thead>
                <tr>
                  <th>Portada</th>
                  <th>Nombre</th>
                  <th>Descripcion</th>
                  <th>duracion</th>
                  <th>acciones</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>

                  <td>
                    <img src="https://sm.ign.com/ign_es/movie/l/logan/logan_jrbx.jpg" class="rounded" width="80px" >
                  </td>
                  
                  <td>
                  
                  </td>
                  
                  <td>Otto</td>
                  
                  <td>@mdo</td>
                  <td>

                    <button class="btn btn-primary" >
                      <i class="fa fa-edit"></i>  
                    </button>

                    <button class="btn btn-danger" >
                      <i class="fa fa-trash-o"></i>  
                    </button>
                    
                  </td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>              
  </div>

</div>
@endsection