@extends('../templates/dashboard/skeleton')

@section('main-page')
<div class="content-inner">
  
  <div class="row p-5">
    <div class="card w-100">
        
        <div class="card-header d-flex align-items-center justify-content-between">
          <h3 class="h4">ACTORS</h3>
          
          <button class="btn btn-success flex">
            <a href="{{ route('newActor') }}" class="text-white" >
              <i class="fa fa-plus"></i> nuevo  
            </a>
          </button>
        
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table overflow-scroll" style="max-height: 700px;" >
              <thead>
                <tr>
                  <th>Actor</th>
                  <th>Nombre</th>
                  <th>acciones</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>

                  <td>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLJ9DkN4vV1S7gmSY_3pvtNdNicbcRV2Q8EXnvkrNw6nD4scJMpUKWus6T00fuSRn36TM&usqp=CAU" class="rounded" width="80px" >
                  </td>
                  
                  <td>Otto</td>
                  
                  <td>

                    <button class="btn btn-primary" >
                      <i class="fa fa-edit"></i>  
                    </button>

                    <button class="btn btn-danger" >
                      <i class="fa fa-trash-o"></i>  
                    </button>
                    
                  </td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>              
  </div>

</div>
@endsection