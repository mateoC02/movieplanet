
@extends('templates.skeleton')

@section('main-content')
    
    <main>
        <div class="grid md:grid-cols-2" >
            
            <div class="" >
                <div class="grid min-h-screen place-items-center">
                    <div class="w-11/12 p-12 bg-white sm:w-8/12 md:w-1/2 lg:w-5/12">
                      <h1 class="text-xl font-semibold">REGISTRARSE</h1>
                      <form class="mt-6">
                        
                        <div class="mb-8" >
                            <label for="email" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Usuario</label>
                            <input id="email" type="email" name="email" placeholder="myuser.05" autocomplete="email" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
                        </div>
                        
                        <div class="mb-3" >
                            <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Contraseña</label>
                            <input id="password" type="password" name="password" placeholder="********" autocomplete="new-password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
                        </div>
                        
                        <button type="submit" class="rounded w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-primary shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
                          Registrarse
                        </button>
                        <p class="flex justify-between mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Ya tienes una cuenta?</p>
                      </form>
                    </div>
                  </div>
            </div>

            <div class=" bg-black h-screen md:flex gap-2 flex-col justify-center items-center" >
                <h2 class="text-white text-5xl" >MOVIES <span class="font-bold" >PLANET</span> </h2>
                <p class="text-white font-light text-sm" >las mejores peliculas en estreno</p>
            </div>
        
        </div>
    </main>

@endsection