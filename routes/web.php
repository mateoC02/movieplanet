<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/info', function () {
    return view('info-movie');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/reserver', function () {
    return view('reserver');
});

Route::get('/signup', function () {
    return view('signup');
});

Route::get('/user', function () {
    return view('user');
});


Route::prefix("admin")->group(function(){

    Route::get('/', function () {
        return view('admin/dashboard');
    })->name("admin");

    Route::get('/movies', function () {
        return view('admin/movies');
    })->name("movies");

    Route::get('/movies/new', function () {
        return view('admin/newmovie');
    })->name("newMovie");


    Route::get('/premier', function () {
        return view('admin/premier');
    })->name("premier");

    Route::get('/premier/new', function () {
        return view('admin/newpremier');
    })->name("newPremier");


    Route::get('/cinema', function () {
        return view('admin/cinemas');
    })->name("cinema");

    Route::get('/cinema/new', function () {
        return view('admin/newcinema');
    })->name("newCinema");


    Route::get('/actors', function () {
        return view('admin/actors');
    })->name("actors");

    Route::get('/actors/new', function () {
        return view('admin/newactors');
    })->name("newActor");


    Route::get('/genres', function () {
        return view('admin/genres');
    })->name("genres");

     Route::get('/genres/new', function () {
        return view('admin/newgenre');
    })->name("newGenres");


    Route::get('/users', function () {
        return view('admin/users');
    })->name("users");

});

